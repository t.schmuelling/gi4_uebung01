#include <stdio.h>

int main(int argc, char* argv[], char* envp[]) 
{
  for(int i = 1; i < argc; i++)
  {
    printf("%s", argv[i]);
    if(i < argc)
    {
      printf(" ");
    }
  }
  printf("\n");

  return 0;
}
